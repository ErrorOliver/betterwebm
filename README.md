# BetterWEBM

Re-adds the Discord WEBM glitch. 

## Getting started
_The idea was made by ErrorOliver and Twisty Nado created the code._

1. Install Stylus for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/) or [Chrome](https://chromewebstore.google.com/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne?pli=1).
2. Go [here](https://userstyles.world/style/16501/betterwebm) and select "install". You should be taken to a menu. Press "Install style" on the top of the left sidebar and you're good to go.

## Previews
![Preview](https://gitlab.com/ErrorOliver/betterwebm/-/raw/main/preview.gif)



License: CC BY-SA 4.0 Deed